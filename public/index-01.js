import Sample01 from "./Sample01.js";
import Sample02 from "./Sample02.js";
import Sample03 from "./Sample03.js";

const block01 = document.querySelector(".js-block-01");
const sample01 = new Sample01();
const sample02 = new Sample02();
const sample03 = new Sample03(block01);

console.log("Add click event from block01.");
block01.addEventListener("click", sample01, false);
block01.addEventListener("click", sample02, false);

setTimeout(() => {
  console.log("Remove click event from block01.");
  block01.removeEventListener("click", sample01, false);
}, 2000);

setTimeout(() => {
  console.log("Remove click event from block01.");
  block01.removeEventListener("click", sample02, false);
}, 5000);

setTimeout(() => {
  console.log("Add mousemove event to block01.");
  sample03.start();
}, 5000);

setTimeout(() => {
  console.log("Remove mousemove event from block01.");
  sample03.stop();
}, 7000);
