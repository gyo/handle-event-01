/**
 * EventListener.handleEvent() を試すためのサンプルクラス
 */
export default class Sample03 {
  /**
   * コンストラクタ
   * @param {HTMLElement} element イベントを設定する要素
   */
  constructor(element) {
    this.element = element;
  }

  /**
   * addEventListenerにthisを渡したときに実行されるメソッド
   * @param {*} e イベント
   * @returns {undefined}
   */
  handleEvent(e) {
    console.log("Sample03", e);
  }

  /**
   * イベント購読開始
   * @return {undefined}
   */
  start() {
    this.element.addEventListener("mousemove", this, false);
  }

  /**
   * イベント購読停止
   * @return {undefined}
   */
  stop() {
    this.element.removeEventListener("mousemove", this, false);
  }
}
