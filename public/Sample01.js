/**
 * EventListener.handleEvent() を試すためのサンプルクラス
 */
export default class Sample01 {
  /**
   * コンストラクタ
   */
  constructor() {}

  /**
   * addEventListenerにthisを渡したときに実行されるメソッド
   * @param {*} e イベント
   * @returns {undefined}
   */
  handleEvent(e) {
    console.log("Sample01", e);
  }
}
