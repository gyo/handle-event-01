/**
 * EventListener.handleEvent() を試すためのサンプルクラス
 */
export default class Sample02 {
  /**
   * コンストラクタ
   */
  constructor() {}

  /**
   * addEventListenerにthisを渡したときに実行されるメソッド
   * @param {*} e イベント
   * @returns {undefined}
   */
  handleEvent(e) {
    console.log("Sample02", e);
  }
}
